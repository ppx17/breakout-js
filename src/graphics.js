class Element {
    constructor(pos) {
        this.pos = pos;
    }

    initialize(ctx) {
    }

    update() {
    }

    draw(ctx) {
    }

    position() {
        return this.pos;
    }
}

class Circle extends Element {
    radius = 10;
    fillStyle = "#0095DD";

    draw(ctx) {
        ctx.beginPath();
        ctx.arc(this.position().x, this.position().y, this.radius, 0, Math.PI * 2);
        ctx.fillStyle = this.fillStyle;
        ctx.fill();
        ctx.closePath();
    }
}

class Polygon extends Element {
    sides = 3;
    radius = 10;
    rotation = 0;
    fillStyle = "#0095DD";

    draw(ctx) {
        if (this.sides < 3 || this.radius < 0) return;
        ctx.beginPath();
        const PI2 = Math.PI * 2;
        const a = (PI2 / this.sides);
        ctx.moveTo(
            this.radius * Math.cos((a * this.sides + this.rotation) % PI2) + this.pos.x,
            this.radius * Math.sin((a * this.sides + this.rotation) % PI2) + this.pos.y
        );
        for (let i = 0; i < this.sides; i++) {
            ctx.lineTo(
                this.radius * Math.cos((a * i + this.rotation) % PI2) + this.pos.x,
                this.radius * Math.sin((a * i + this.rotation) % PI2) + this.pos.y
            );
        }
        ctx.closePath();
        ctx.fillStyle = this.fillStyle;
        ctx.fill();
    }
}

class Rectangle extends Element {
    fillStyle = "#0095DD";

    constructor(pos, size) {
        super(pos);
        this.width = size.x;
        this.height = size.y;
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.rect(this.position().x, this.position().y, this.width, this.height);
        ctx.fillStyle = this.fillStyle;
        ctx.fill();
        ctx.closePath();
    }
}

class Text extends Element {
    text = "";
    fillStyle = "#000000";
    font = "10px Georgia";

    draw(ctx) {
        ctx.font = this.font;
        ctx.fillStyle = this.fillStyle;
        ctx.fillText(this.text, this.pos.x, this.pos.y);
    }
}