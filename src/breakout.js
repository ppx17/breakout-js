class Breakout {
    bonusTypes = [ExtraBallBonus, SuperBallBonus, SmallPaddleBonus, WidePaddleBonus];
    bonuses = [];
    balls = [];

    constructor(game) {
        this.brickRows = 8;
        this.brickCols = 15;
        this.brickSize = new Vector((game.canvas.width / ((this.brickCols + 0.3) * 1.125)), Math.floor(game.canvas.height / 40));
        this.game = game;
        this.game.events.addEventListener("postupdate", this.update.bind(this));
        this.cd = new CollisionDetector();
        this.spawnPaddle();
        this.spawnBricks();
        this.spawnBall();
    }

    spawnPaddle() {
        this.paddle = new Paddle(
            new Vector(
                (this.game.canvas.width / 2) - this.brickSize.x / 2,
                this.game.canvas.height - this.brickSize.y - 5
            ),
            this.brickSize,
            this.game
        );
        this.game.addObject(this.paddle);
    }

    spawnBricks() {
        this.brickFactory = new BrickFactory(this.brickRows, this.brickCols, this.brickSize);
        this.brickFactory.bricks.forEach(b => this.game.addObject(b));
    }

    spawnBall() {
        const ball = new Ball(new Vector(this.game.canvas.width / 2, this.game.canvas.height / 2), this.game);
        this.balls.push(ball);
        this.game.addObject(ball);
    }

    update() {
        this.updateBall();
        this.collideWithBricks();
        this.collideWithBonus();
        this.cleanOldBonuses();
    }

    updateBall() {
        this.balls.forEach(ball => {
            if (this.cd.circleHitsRectangle(ball, this.paddle) && ball.vel.y > 0) {
                const half = this.paddle.width / 2;
                const relX = ((ball.position().x - this.paddle.position().x) - half) / half;
                ball.vel.x = relX * 2;
                ball.vel.y = -(4 - Math.abs(relX));
            }
            if (ball.position().y + ball.radius >= this.game.canvas.height) {
                this.balls = this.balls.filter(o => o !== ball);
                this.game.removeObject(ball);
            }
            if (this.balls.length === 0) {
                this.game.gameOver();
            }
        });
    }

    collideWithBricks() {
        this.balls.forEach(ball => {
            this.brickFactory.bricks.forEach(brick => {
                if (brick.active
                    && this.cd.circleHitsRectangle(ball, brick)) {
                    ball.hitBrick();
                    this.game.events.dispatchEvent("bricked");
                    brick.hit();
                    if (this.brickFactory.noBricksLeft()) {
                        this.game.gameFinished();
                    }
                }
            });
        });
    }

    collideWithBonus() {
        this.bonuses.forEach(bonus => {
            if (this.cd.circleHitsRectangle(bonus, this.paddle)) {
                bonus.caught();
                this.bonuses = this.bonuses.filter(o => o !== bonus);
                this.game.removeObject(bonus);
            }
        });
    }

    cleanOldBonuses() {
        this.bonuses.forEach(bonus => {
            if (bonus.position().y > this.game.canvas.height) {
                this.bonuses = this.bonuses.filter(o => o !== bonus);
                this.game.removeObject(bonus);
            }
        })
    }

    spawnBonus(pos) {
        const type = randomElement(this.bonusTypes);
        const b = new type(pos);
        this.bonuses.push(b);
        this.game.addObject(b);
    }
}
class BrickFactory {

    brickTypes = [
        RegularBrick,
        HardBrick,
        BonusBrick
    ];

    constructor(rows, columns, brickSize) {
        this.brickSize = brickSize;
        this.rows = rows;
        this.columns = columns;
        this.offsetTop = brickSize.y * 4;
        this.offsetLeft = brickSize.x * 0.25;
        this.width = brickSize.x;
        this.height = brickSize.y;
        this.padding = brickSize.x * 0.125;
        this.bricks = [];
        this.generate();
    }

    generate() {
        for (let row = 0; row < this.rows; row++) {
            for (let col = 0; col < this.columns; col++) {
                const type = randomElement(this.brickTypes);
                this.bricks.push(new type(new Vector(
                    this.offsetLeft + (col * (this.width + this.padding)),
                    this.offsetTop + (row * (this.height + this.padding))
                ), this.brickSize));
            }
        }
    }

    noBricksLeft() {
        return this.bricks.filter(b => b.active).length === 0;
    }
}

class AutoPilot {
    engaged = false;
    targetBonuses = true;
    currentTarget = null;

    constructor(rules) {
        this.rules = rules;
    }

    initialize() {

    }

    update() {
        if (!this.engaged) return;

        this.currentTarget = this.target();

        if (this.currentTarget === null) return;

        this.rules.paddle.rightPressed = this.currentTarget.position().x > this.rules.paddle.position().x + (this.rules.paddle.width * 0.75);
        this.rules.paddle.leftPressed = this.currentTarget.position().x < (this.rules.paddle.position().x + this.rules.paddle.width * 0.25);
    }

    target() {
        const downwardBalls = this.rules.balls.filter(b => b.vel.y > 0).sort((a, b) => a.position().y < b.position().y ? 1 : -1);
        if (downwardBalls.length > 0) {
            return downwardBalls[0];
        }

        if (this.targetBonuses) {
            const bonuses = this.rules.bonuses.sort((a, b) => a.position().y < b.position().y ? 1 : -1);
            if (bonuses.length > 0) {
                return bonuses[0];
            }
        }

        const upwardBalls = this.rules.balls.filter(b => b.vel.y < 0).sort((a, b) => a.position().y > b.position().y ? 1 : -1);
        if (upwardBalls.length > 0) {
            return upwardBalls[0];
        }
        return null;
    }

    draw(ctx) {
        if (this.currentTarget === null) return;
        if (!this.engaged) return;

        const x = this.currentTarget.position().x;
        const y = this.currentTarget.position().y;

        ctx.beginPath();
        ctx.strokeStyle = "#FF0000";
        ctx.arc(x, y, 15, 0, Math.PI * 2);
        ctx.stroke();
        ctx.moveTo(x - 15, y - 15);
        ctx.lineTo(x + 15, y + 15);
        ctx.stroke();
        ctx.moveTo(x + 15, y - 15);
        ctx.lineTo(x - 15, y + 15);
        ctx.stroke();
        ctx.closePath();
    }

    disengage() {
        this.engaged = false;
        this.rules.paddle.rightPressed = false;
        this.rules.paddle.leftPressed = false;
    }

    engage() {
        this.engaged = true;
    }
}

