class Brick extends Rectangle {
    fillStyle = "#184d66";
    active = true;

    draw(ctx) {
        if (this.active) {
            super.draw(ctx);
        }
    }

    hit() {
        this.active = false;
    }
}
class BonusBrick extends Brick {
    initialize(ctx) {
        super.initialize(ctx);
        this.gradient = ctx.createLinearGradient(this.pos.x, this.pos.y, this.pos.x, this.pos.y + this.height);
        this.gradient.addColorStop(0, "#b86568");
        this.gradient.addColorStop(1, "#890923");
        this.fillStyle = this.gradient;
    }

    hit() {
        breakout.spawnBonus(new Vector(this.pos.x + this.width / 2, this.pos.y + this.height / 2));
        super.hit();
    }
}
class RegularBrick extends Brick {
    initialize(ctx) {
        super.initialize(ctx);
        this.gradient = ctx.createLinearGradient(this.pos.x, this.pos.y, this.pos.x, this.pos.y + this.height);
        this.gradient.addColorStop(0, "#9acedc");
        this.gradient.addColorStop(1, "#184d66");
        this.fillStyle = this.gradient;
    }

    draw(ctx) {
        if (this.active) {
            super.draw(ctx);
        }
    }
}

class HardBrick extends Brick {
    isBrittle = false

    initialize(ctx) {
        super.initialize(ctx);
        this.gradient = ctx.createLinearGradient(this.pos.x, this.pos.y, this.pos.x, this.pos.y + this.height);
        this.gradient.addColorStop(0, "#162521");
        this.gradient.addColorStop(1, "#253e39");
        this.fillStyle = this.gradient;
    }

    hit() {
        if (this.isBrittle) {
            super.hit();
        } else {
            this.isBrittle = true;
            this.gradient.addColorStop(0, "#637379");
            this.gradient.addColorStop(1, "#3C474B");
        }
    }
}

// Bonuses

class Bonus {
    primaryColor = "#000000";
    secondaryColor = "#FFFFFF";
    radius = 10;

    constructor(pos) {
        this.polygon = new Polygon(pos);
        this.polygon.sides = 5;
        this.polygon.radius = this.radius;
        this.velocity = new Vector(0, 1);
    }

    draw(ctx) {
        const gradient = ctx.createRadialGradient(this.position().x, this.position().y, 0, this.position().x, this.position().y, this.polygon.radius * 2);
        gradient.addColorStop(0, this.primaryColor);
        gradient.addColorStop(1, this.secondaryColor);

        this.polygon.rotation += 0.02 * Math.PI;
        this.polygon.fillStyle = gradient;
        this.polygon.draw(ctx);
    }

    update(t) {
        this.polygon.pos.add(this.velocity.time(t));
    }

    position() {
        return this.polygon.pos;
    }
}

class SmallPaddleBonus extends Bonus {
    primaryColor = "#e0c0c0";
    secondaryColor = "#ac4f4f";

    caught() {
        breakout.paddle.small();
    }
}

class SuperBallBonus extends Bonus {
    primaryColor = "#DEE0C0";
    secondaryColor = "#AC7C4F";

    caught() {
        breakout.balls.forEach(ball => {
            if (!ball.isUnstoppable && Math.random() > 0.5) {
                ball.unstoppable()
            }
        });
    }
}

class WidePaddleBonus extends Bonus {
    primaryColor = "#cfe0c0";
    secondaryColor = "#6bac4f";

    caught() {
        breakout.paddle.wide();
    }
}

class ExtraBallBonus extends Bonus {
    primaryColor = "#d5c0e0";
    secondaryColor = "#854fac";

    caught() {
        breakout.spawnBall();
    }
}


class Ball extends Circle {
    isUnstoppable = false;

    constructor(pos, game) {
        super(pos);
        this.canvas = game.canvas;
        const rnd = Math.random() * 1.5;
        this.vel = new Vector(rnd, 4 - rnd);
    }

    unstoppable() {
        this.isUnstoppable = true;
        setTimeout(this.stoppable.bind(this), 5000);
    }

    stoppable() {
        this.isUnstoppable = false;
    }

    update(t) {
        this.pos.add(this.vel.time(t));
        if (this.pos.y < this.radius || this.pos.y + this.radius > this.canvas.height) {
            this.vel.y = -this.vel.y;
        }
        if (this.pos.x < this.radius || this.pos.x + this.radius > this.canvas.width) {
            this.vel.x = -this.vel.x;
        }
    }

    draw(ctx) {
        const gradient = ctx.createRadialGradient(this.pos.x, this.pos.y, 0, this.pos.x, this.pos.y, this.radius * 2);
        if (!this.isUnstoppable) {
            gradient.addColorStop(0, "#C0E0DE");
            gradient.addColorStop(1, "#4F7CAC");
        } else {
            gradient.addColorStop(0, "#DEE0C0");
            gradient.addColorStop(1, "#AC7C4F");
        }
        this.fillStyle = gradient;
        super.draw(ctx);
    }

    hitBrick() {
        if (!this.isUnstoppable) {
            this.vel.y = -this.vel.y;
        }
    }
}

class Paddle {
    rightPressed = false;
    leftPressed = false;
    sizeTimer = null;

    constructor(pos, size, game) {
        this.baseSize = size;
        this.element = new Rectangle(pos.clone(), size);
        this.leftIndicator = new Circle(pos.clone());
        this.rightIndicator = new Circle(pos.clone());
        this.leftIndicator.radius = size.y * 0.55;
        this.leftIndicator.rotation = Math.PI;
        this.rightIndicator.radius = size.y * 0.55;
        this.width = size.x;
        this.height = size.y;
        this.canvas = game.canvas;
        document.addEventListener("keydown", this.keyDownHandler.bind(this), true);
        document.addEventListener("keyup", this.keyUpHandler.bind(this), true);
    }

    initialize(ctx) {
        this.element.initialize(ctx);
    }

    draw(ctx) {
        this.element.width = this.width;
        this.element.height = this.height;
        this.drawIndicators(ctx);
        this.element.draw(ctx);
    }

    drawIndicators(ctx) {
        this.leftIndicator.pos = this.element.pos.clone();
        this.leftIndicator.pos.add(new Vector(-this.leftIndicator.radius * 0.35, this.leftIndicator.radius * 0.9))
        this.rightIndicator.pos = this.element.pos.clone();
        this.rightIndicator.pos.add(new Vector(this.width + this.rightIndicator.radius * 0.35, this.rightIndicator.radius * 0.9));
        this.leftIndicator.fillStyle = this.indicatorFill(this.leftPressed);
        this.rightIndicator.fillStyle = this.indicatorFill(this.rightPressed);
        this.leftIndicator.draw(ctx);
        this.rightIndicator.draw(ctx);
    }

    indicatorFill(state) {
        return state ? "#0e2c44" : "#026595";
    }

    keyUpHandler(e) {
        if (e.key === "Right" || e.key === "ArrowRight") {
            this.rightPressed = false;
        } else if (e.key === "Left" || e.key === "ArrowLeft") {
            this.leftPressed = false;
        }
    }

    keyDownHandler(e) {
        if (e.key === "Right" || e.key === "ArrowRight") {
            this.rightPressed = true;
        } else if (e.key === "Left" || e.key === "ArrowLeft") {
            this.leftPressed = true;
        }
    }

    update(t) {
        if (this.leftPressed) {
            this.element.pos.x -= 7 * t;
        }
        if (this.rightPressed) {
            this.element.pos.x += 7 * t;
        }
        if (this.element.pos.x < 0) {
            this.element.pos.x = 0;
        }
        if (this.element.pos.x + this.width > this.canvas.width) {
            this.element.pos.x = this.canvas.width - this.width;
        }
    }

    position() {
        return this.element.pos;
    }

    clearSizeTimer() {
        if (this.sizeTimer !== null) {
            clearTimeout(this.sizeTimer);
        }
    }

    wide() {
        this.clearSizeTimer();
        this.width = this.baseSize.x * 2;
        this.sizeTimer = setTimeout(this.normal.bind(this), 7000);
    }

    normal() {
        this.width = this.baseSize.x;
    }

    small() {
        this.clearSizeTimer();
        this.width = this.baseSize.x / 2;
        this.sizeTimer = setTimeout(this.normal.bind(this), 3000);
    }
}
class Score {
    score = 0;

    constructor(game) {
        this.text = new Text(new Vector(10, 20));
        this.text.fillStyle = "#0e2c44"
        this.text.font = "18px Verdana"
        game.events.addEventListener("bricked", this.bricked.bind(this));
    }

    initialize() {

    }

    update() {

    }

    bricked() {
        this.score += 1;
    }

    draw(ctx) {
        this.text.text = `Score: ${this.score}`;
        this.text.draw(ctx);
    }
}