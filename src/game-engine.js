
class CollisionDetector {
    circleHitsRectangle(circle, rectangle) {
        return this.circleRect(
            circle.position().x, circle.position().y, circle.radius,
            rectangle.position().x, rectangle.position().y, rectangle.width, rectangle.height
        );
    }

    circleRect(cx, cy, radius, rx, ry, rw, rh) {
        let testX = cx;
        if (cx < rx) testX = rx;
        if (cx > rx + rw) testX = rx + rw;

        let testY = cy;
        if (cy < ry) testY = ry;
        if (cy > ry + rh) testY = ry + rh;

        const distX = cx - testX;
        const distY = cy - testY;
        const distance = Math.sqrt((distX * distX) + (distY * distY));

        return distance <= radius;
    }
}
class EventHandler {
    eventListeners = {};

    addEventListener(event, handler) {
        if (this.eventListeners[event] === undefined) {
            this.eventListeners[event] = [];
        }
        this.eventListeners[event].push(handler);
    }

    dispatchEvent(event, data) {
        if (this.eventListeners[event] !== undefined) {
            this.eventListeners[event].forEach(x => x(data));
        }
    }
}
class Game {
    isRunning = true;
    objects = [];

    constructor(canvas, ctx) {
        this.ctx = ctx;
        this.canvas = canvas;
        this.events = new EventHandler();
    }

    initializeGraphics() {
        this.objects.forEach(o => o.initialize(this.ctx));
    }

    update(timeFactor) {
        this.events.dispatchEvent("preupdate");
        this.objects.forEach(o => o.update(timeFactor));
        this.events.dispatchEvent("postupdate");
    }

    draw(timeFactor) {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.objects.forEach(o => o.draw(this.ctx, timeFactor));
    }

    addObject(object) {
        this.objects.push(object);
    }

    removeObject(object) {
        this.objects = this.objects.filter(o => o !== object);
    }

    gameOver() {
        this.isRunning = false;
        this.events.dispatchEvent("gameover");
        this.events.dispatchEvent("gamestopped");
    }

    gameFinished() {
        this.isRunning = false;
        this.events.dispatchEvent("gamefinished");
        this.events.dispatchEvent("gamestopped");
    }
}
class GameEngine {
    constructor(game) {
        this.isRunning = false;
        this.game = game;
        this.drawTime = 0;
        this.updateTime = 0;
        this.totalFrames = 0;
        this.maxFrameTime = 0;
        this.currentFps = 0;
        this.runningFpsCount = 0;
        this.currentTimeFactor = 1;
    }

    setTargetFrameRate(targetFps) {
        if(targetFps === 0) {
            this.maxFrameTime = 0;
        }else {
            this.maxFrameTime = 1000 / targetFps;
        }
        if(this.isRunning) {
            this.stop();
            this.start();
        }
    }

    initialize() {
        this.game.initializeGraphics();
    }

    start() {
        this.tickInterval = setInterval(this.tick.bind(this), this.maxFrameTime);
        this.fpsInterval = setInterval(this.updateFps.bind(this), 250);
        this.isRunning = true;
    }

    stop() {
        clearInterval(this.tickInterval);
        clearInterval(this.fpsInterval);
        this.isRunning = false;
    }

    tick() {
        const t0 = performance.now();
        this.game.update(this.currentTimeFactor);
        const t1 = performance.now();
        this.game.draw(this.currentTimeFactor);
        const t2 = performance.now();
        this.updateTime = t1 - t0;
        this.drawTime = t2 - t2;
        this.totalFrames++;
        this.runningFpsCount++;
    }

    updateFps() {
        this.currentFps = this.runningFpsCount * 4;
        this.runningFpsCount = 0;
        this.currentTimeFactor = (this.currentFps > 0) ? 100 / this.currentFps : 1;
    }
}

randomElement = (list) => list[Math.floor(Math.random() * list.length)];

class Vector {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    add(vector) {
        this.x += vector.x;
        this.y += vector.y;
    }

    time(factor) {
        return new Vector(this.x * factor, this.y * factor);
    }

    clone() {
        return new Vector(this.x, this.y);
    }
}